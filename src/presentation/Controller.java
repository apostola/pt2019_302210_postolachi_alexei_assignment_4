package presentation;

import BLL.Restaurant;
import data.CompositeProduct;
import data.MenuItem;
import data.Order;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class Controller {
    private AdminUI adminUI;
    private ChefUI chefUI;
    private WaiterUI waiterUI;
    private Restaurant restaurant = new Restaurant();

    public Controller(AdminUI adminUI, WaiterUI waiterUI) throws IOException, ClassNotFoundException {
        this.adminUI = adminUI;
        this.chefUI = new ChefUI(this);
        this.waiterUI = waiterUI;

        restaurant.addObserver(chefUI);

        adminUI.setMenu(restaurant.getMenu());
        adminUI.populateMenuTable();
        waiterUI.setMenu(restaurant.getMenu());
        waiterUI.populateMenuTable();

        adminUI.addSubmitListener(new addMenuItemListener());
        adminUI.addDeleteListener(new deleteMenuItemListener());
        adminUI.addEditListener(new editMenuItemListener());
        adminUI.addCompositeListener(new addCompositeItemListener());
        adminUI.addSaveListener(new serialize());

        waiterUI.addAddListener(new addItemToOrder());
        waiterUI.addSubmitListener(new placeOrder());
    }

    public ArrayList<Order> getOrders() {
        return restaurant.getOrders();
    }

    public class addMenuItemListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String name = adminUI.getName();
            int price = Integer.parseInt(adminUI.getPrice());

            restaurant.addItemToMenu(new MenuItem(name, price));

            waiterUI.populateMenuTable();
            adminUI.populateMenuTable();
        }
    }

    public class deleteMenuItemListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String name = adminUI.getName();
            int price = Integer.parseInt(adminUI.getPrice());

            restaurant.deleteItemFromMenu(new MenuItem(name, price));

            waiterUI.populateMenuTable();
            adminUI.populateMenuTable();
        }
    }

    public class editMenuItemListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String name = adminUI.getName();
            String newName = adminUI.getNewName();
            int price = Integer.parseInt(adminUI.getPrice());

            restaurant.updateItem(name, newName, price);

            waiterUI.populateMenuTable();
            adminUI.populateMenuTable();
        }
    }

    public class addCompositeItemListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String name = adminUI.getName();
            String othreName = adminUI.getNewName();

            ArrayList<MenuItem> composite = new ArrayList<>();
            MenuItem item1 = restaurant.getItem(name);
            MenuItem item2 = restaurant.getItem(othreName);

            if (item1 != null && item2 != null) {
                composite.add(item1);
                composite.add(item2);
                CompositeProduct compositeProduct = new CompositeProduct(composite, item1.getName() + " cu " + item2.getName(), item1.getPrice() + item2.getPrice());
                restaurant.addItemToMenu(compositeProduct);
                waiterUI.populateMenuTable();
                adminUI.populateMenuTable();
            } else {
                adminUI.showMessage("Error on finding one of items");
            }

        }
    }

    public class addItemToOrder implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String name = waiterUI.getNameProduct();

            MenuItem foundItem = restaurant.getItem(name);

            if (foundItem != null) {
                waiterUI.putItem(foundItem);
            } else {
                waiterUI.showMessage("No such item.");
            }
        }
    }

    public class placeOrder implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ArrayList<MenuItem> items = waiterUI.placeOrder();
            Order order = restaurant.putOrder(items);

            try {
                restaurant.Receipt(order);
            } catch (IOException ex) {
                waiterUI.showMessage("Error while making bill!");
                ex.printStackTrace();
            }
            waiterUI.computePrice();
            waiterUI.newOrder();
            waiterUI.resetOrders();
        }
    }

    public class serialize implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                restaurant.saveMenuToFile();
                adminUI.showMessage("Meniu salvat cu succes!");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
