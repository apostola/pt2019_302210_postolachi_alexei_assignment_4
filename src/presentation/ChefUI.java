package presentation;

import data.MenuItem;
import data.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ChefUI extends JFrame implements Observer {
    private JPanel window;
    private JPanel tableWrapper;
    private JPanel buttonWrapper;

    private JTable ordersTable;
    private DefaultTableModel defaultTableModel;
    private JScrollPane orderTableScrollPane;

    private ArrayList<Order> orders;
    private Controller controller;

    public ChefUI(Controller controller) {
        this.controller = controller;
        window = new JPanel();

        setContent();
        setTable();
        this.setContentPane(window);
        this.setTitle("Chef Panel");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setSize(500, 580);
        this.setVisible(true);
    }

    protected void setContent() {
        window.setLayout(new BoxLayout(window, BoxLayout.Y_AXIS));

        tableWrapper = new JPanel();
        window.add(tableWrapper);

        buttonWrapper = new JPanel();
        window.add(buttonWrapper);
    }

    protected void setTable() {
        ordersTable = new JTable(5, 2);
        orderTableScrollPane = new JScrollPane();

        defaultTableModel = new DefaultTableModel(null, new String[] {"id", "Nume"});
        defaultTableModel.setColumnIdentifiers(new Object[] {"id" , "Nume"});

        orderTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        orderTableScrollPane.setViewportView(ordersTable);

        ordersTable.setModel(defaultTableModel);
        tableWrapper.add(orderTableScrollPane);
    }

    public void reset() { defaultTableModel.setRowCount(0); }

    protected void addOrder(Order order) {
        ArrayList<MenuItem> orderItems = order.getTable();
        for (MenuItem itemFromMenu : orderItems) {
            defaultTableModel.addRow(new Object[] {order.getOrderID(), itemFromMenu.getName()});
        }
    }

    protected void addOrders() {
        reset();
        for (Order order : orders) {
            addOrder(order);
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        orders = controller.getOrders();
        addOrders();
        JOptionPane.showMessageDialog(window, "New order added into the queue");
    }
}
