package presentation;

import data.IRestaurantProcessing;
import data.MenuItem;
import data.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdminUI extends JFrame implements IRestaurantProcessing {

    private JPanel window;
    private JPanel menuTableWrapper;

    private JPanel buttonWrapper;
    private JTextField inputForName;
    private JTextField inputForPrice;
    private JTextField inputForNewName;
    private JButton buttonForDelete;
    private JButton buttonForSubmit;
    private JButton buttonForEdit;
    private JButton buttonForComposite;
    private JButton buttonForSaveMenu;

    private JTable menuTable;
    private DefaultTableModel menuTableModel;
    private JScrollPane menuTableScrollPane;

    private ArrayList<MenuItem> menu;

    public AdminUI() {
        window = new JPanel();

        setContent();
        setTable();
        this.setContentPane(window);
        this.setTitle("Admin Panel");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setSize(500, 580);
        this.setVisible(true);
    }

    protected void setContent() {
        window.setLayout(new BoxLayout(window, BoxLayout.Y_AXIS));

        menuTableWrapper = new JPanel();
        window.add(menuTableWrapper);

        buttonWrapper = new JPanel();
        window.add(buttonWrapper);

        JPanel panelForTextFields = new JPanel();
        panelForTextFields.setLayout(new BoxLayout(panelForTextFields, BoxLayout.Y_AXIS));
        inputForName = new JTextField(15);
        inputForPrice = new JTextField(15);;
        inputForNewName = new JTextField( 15);
        panelForTextFields.add(inputForName);
        panelForTextFields.add(inputForPrice);
        panelForTextFields.add(inputForNewName);

        JPanel panelForButtons = new JPanel();
        panelForButtons.setLayout(new GridLayout(4, 1));
        buttonForDelete = new JButton("DELETE");
        buttonForSubmit = new JButton("SUBMIT");
        buttonForEdit = new JButton("EDIT");
        buttonForComposite = new JButton("Create Composite\n");
        buttonForSaveMenu = new JButton("Save menu");
        panelForButtons.add(buttonForSubmit);
        panelForButtons.add(buttonForEdit);
        panelForButtons.add(buttonForDelete);
        panelForButtons.add(buttonForSaveMenu);

        buttonWrapper.add(panelForTextFields);
        buttonWrapper.add(buttonForComposite);
        buttonWrapper.add(panelForButtons);
    }

    protected void setTable() {
        menuTable = new JTable(5, 2);
        menuTableScrollPane = new JScrollPane();

        menuTableModel = new DefaultTableModel(null, new String[] {"Nume", "Pret"});
        menuTableModel.setColumnIdentifiers(new Object[] {"Nume", "Pret"});

        menuTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        menuTableScrollPane.setViewportView(menuTable);

        menuTable.setModel(menuTableModel);
        menuTableWrapper.add(menuTableScrollPane);
    }

    @Override
    public void addItemToMenu(MenuItem item) { menu.add(item); }
    @Override
    public void deleteItemFromMenu(MenuItem item) { menu.remove(item); }
    @Override
    public Order putOrder(ArrayList<MenuItem> items) {return null;}

    public void resetMenu() { menuTableModel.setRowCount(0); }

    public void setMenu(ArrayList<MenuItem> menu) { this.menu = menu; }

    public void populateMenuTable() {
        resetMenu();
        for (MenuItem menuItem: menu) {
            menuTableModel.addRow(new Object[] {menuItem.getName(), menuItem.getPrice()});
        }
        this.setVisible(true);
    }

    public void showMessage(String message) {
        JOptionPane.showMessageDialog(window, message);
    }

    public void addSubmitListener(ActionListener e) {buttonForSubmit.addActionListener(e);}
    public void addEditListener(ActionListener e) {buttonForEdit.addActionListener(e);}
    public void addDeleteListener(ActionListener e) {buttonForDelete.addActionListener(e);}
    public void addCompositeListener(ActionListener e) {buttonForComposite.addActionListener(e);}
    public void addSaveListener(ActionListener e) {buttonForSaveMenu.addActionListener(e);}
    public String getName() {return inputForName.getText();}
    public String getNewName() {return inputForNewName.getText();}
    public String getPrice() {return inputForPrice.getText();}


}
