package presentation;

import data.IRestaurantProcessing;
import data.MenuItem;
import data.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class WaiterUI extends  JFrame  implements IRestaurantProcessing {
    private JPanel window;
    private JPanel upperPannel;
    private JPanel menuTableWrapper;
    private JPanel orderTableWrapper;

    private JPanel buttonWrapper;
    private JTextField productName;
    private JButton addItem;
    private JButton submitOrder;

    private JTable ordersTable;
    private JTable menuTable;
    private DefaultTableModel orderTableModel;
    private JScrollPane orderTableScrollPane;
    private DefaultTableModel menuTableModel;
    private JScrollPane menuTableScrollPane;

    private ArrayList<MenuItem> currentOrder;
    private ArrayList<MenuItem> menu;

    public WaiterUI() {
        window = new JPanel();
        currentOrder = new ArrayList<>();
        setContent();
        setTable();
        this.setContentPane(window);
        this.setTitle("Waiter Panel");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setSize(1000, 550);
        this.setVisible(true);
    }

    protected void setContent() {
        window.setLayout(new BoxLayout(window, BoxLayout.Y_AXIS));

        upperPannel = new JPanel();
        upperPannel.setLayout(new GridLayout(1, 2));

        menuTableWrapper = new JPanel();
        upperPannel.add(menuTableWrapper);
        orderTableWrapper = new JPanel();
        upperPannel.add(orderTableWrapper);

        window.add(upperPannel);

        buttonWrapper = new JPanel();
        productName = new JTextField(15);
        addItem = new JButton("Add Item");
        submitOrder = new JButton("Submit order");
        buttonWrapper.add(productName);
        buttonWrapper.add(addItem);
        buttonWrapper.add(submitOrder);

        window.add(buttonWrapper);
    }

    protected void setTable() {
        ordersTable = new JTable(5, 2);
        orderTableScrollPane = new JScrollPane();

        orderTableModel = new DefaultTableModel(null, new String[] {"Nume", "Pret"});
        orderTableModel.setColumnIdentifiers(new Object[] {"Nume", "Pret"});

        orderTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        orderTableScrollPane.setViewportView(ordersTable);

        ordersTable.setModel(orderTableModel);
        orderTableWrapper.add(orderTableScrollPane);

        menuTable = new JTable(5, 2);
        menuTableScrollPane = new JScrollPane();

        menuTableModel = new DefaultTableModel(null, new String[] {"Nume", "Pret"});
        menuTableModel.setColumnIdentifiers(new Object[] {"Nume", "Pret"});

        menuTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        menuTableScrollPane.setViewportView(menuTable);

        menuTable.setModel(menuTableModel);
        menuTableWrapper.add(menuTableScrollPane);
    }

    public void resetOrders() { orderTableModel.setRowCount(0); }
    protected void resetMenu() { menuTableModel.setRowCount(0); }

    @Override
    public void addItemToMenu(MenuItem item) {}

    @Override
    public void deleteItemFromMenu(MenuItem item) {}

    @Override
    public Order putOrder(ArrayList<MenuItem> items) {
        return null;
    }

    public void setMenu(ArrayList<MenuItem> menu) { this.menu = menu; }

    public void populateMenuTable() {
        resetMenu();
        for (MenuItem menuItem: menu) {
            menuTableModel.addRow(new Object[] {menuItem.getName(), menuItem.getPrice()});
        }
    }

    public void putItem(MenuItem item) {
        orderTableModel.addRow(new Object[] {item.getName(), item.getPrice()});
        currentOrder.add(item);
    }
    public ArrayList<MenuItem> placeOrder() { return currentOrder; }
    public void newOrder() { currentOrder = new ArrayList<MenuItem>(); }
    public void computePrice() {
        int price = 0;
        for (MenuItem item: currentOrder) {
            price += item.computePrice();
        }
        JOptionPane.showMessageDialog(window, "Computed Bill Price: " + price);
    }

    public String getNameProduct() { return productName.getText();}

    public void addSubmitListener(ActionListener e) {submitOrder.addActionListener(e);}
    public void addAddListener(ActionListener e) {addItem.addActionListener(e);}

    public void showMessage(String message) {
        JOptionPane.showMessageDialog(window, message);
    }
}
