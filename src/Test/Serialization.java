package Test;

import data.BaseProduct;
import data.CompositeProduct;
import data.MenuItem;

import java.io.*;
import java.util.ArrayList;

public class Serialization {
//    public static void main(String[] args) {
//        BaseProduct product0 = new BaseProduct("Oua", 10);
//        BaseProduct product1 = new BaseProduct("Lapte", 10);
//        BaseProduct product2 = new BaseProduct("Lapte", 10);
//
//        ArrayList<BaseProduct> list = new ArrayList<BaseProduct>();
//        list.add(product0);
//        list.add(product1);
//        list.add(product2);
//
//        CompositeProduct compositeProduct = new CompositeProduct(list);
//
//        try {
//            File f = new File("./tmp/composite_product.txt");
//            FileOutputStream fileOut =
//                    new FileOutputStream(f);
//            ObjectOutputStream out = new ObjectOutputStream(fileOut);
//            out.writeObject(compositeProduct);
//            out.close();
//            fileOut.close();
//            System.out.println("Serialized data is saved in /tmp/composite_product.txt");
//        } catch (IOException i) {
//            i.printStackTrace();
//        }
//
//        CompositeProduct compositeProduct1 = new CompositeProduct();
//
//        try {
//            FileInputStream fileIn = new FileInputStream("./tmp/composite_product.txt");
//            ObjectInputStream in = new ObjectInputStream(fileIn);
//            compositeProduct1 = (CompositeProduct)in.readObject();
//            in.close();
//            fileIn.close();
//        } catch (IOException i) {
//            i.printStackTrace();
//            return;
//        } catch (ClassNotFoundException c) {
//            System.out.println("CompositeProduct class not found");
//            c.printStackTrace();
//            return;
//        }
//
//        System.out.println(compositeProduct1.toString());
//
//
//        MenuItem menuItem = new MenuItem("Shaorma", compositeProduct);
//        System.out.println(menuItem.toString());
//
//        try {
//            File f = new File("./tmp/menu_item.txt");
//            FileOutputStream fileOut =
//                    new FileOutputStream(f);
//            ObjectOutputStream out = new ObjectOutputStream(fileOut);
//            out.writeObject(menuItem);
//            out.close();
//            fileOut.close();
//            System.out.println("Serialized data is saved in /tmp/menu_item.txt");
//        } catch (IOException i) {
//            i.printStackTrace();
//        }
//
//        MenuItem menuItem11 = new MenuItem();
//
//        try {
//            FileInputStream fileIn = new FileInputStream("./tmp/menu_item.txt");
//            ObjectInputStream in = new ObjectInputStream(fileIn);
//            menuItem11 = (MenuItem) in.readObject();
//            in.close();
//            fileIn.close();
//        } catch (IOException i) {
//            i.printStackTrace();
//            return;
//        } catch (ClassNotFoundException c) {
//            System.out.println("MenuItem class not found");
//            c.printStackTrace();
//            return;
//        }
//
//        System.out.println(menuItem11.toString());
//
//    }
}
