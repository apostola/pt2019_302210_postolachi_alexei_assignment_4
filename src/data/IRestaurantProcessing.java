package data;

import com.google.java.contract.Requires;

import java.util.ArrayList;

public interface IRestaurantProcessing {
    @Requires("Item != null")
    void addItemToMenu(MenuItem item);

    @Requires("Item != null")
    void deleteItemFromMenu(MenuItem item);

    @Requires("!items.isEmpty()")
    Order putOrder(ArrayList<MenuItem> items);
}
