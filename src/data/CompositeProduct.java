package data;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements Product, java.io.Serializable{
    int nrComponents;
    private ArrayList<MenuItem> products;

    public CompositeProduct() {
        super("", 0);
        nrComponents = 0;
        products = null;
    }

    public CompositeProduct(ArrayList<MenuItem> list, String nume, int price) {
        super(nume, price);
        nrComponents = list.size();
        products = list;
    }

    public void addProduct(MenuItem item) {
        products.add(item);
        nrComponents = products.size();
    }

    public void setProducts(ArrayList<MenuItem> products) {
        this.products = products;
        nrComponents = products.size();
    }

    public void addProduct(BaseProduct product) {
        products.add(product);
        nrComponents = products.size();
    }


    public MenuItem getProduct(int index) {return products.get(index);}
    public ArrayList<MenuItem> getProducts() {return products;}
    public int getNrComponents() { return nrComponents; }

    @Override
    public int computePrice() {
        int sum = 0;

        for(MenuItem elem: products) {
            sum += elem.getPrice();
        }

        return sum;
    }

    public String toString() {
        String out = "";
        for (MenuItem product: products) {
            out += product.toString() + "\n";
        }
        return out;
    }
}
