package data;

public class BaseProduct extends MenuItem implements Product, java.io.Serializable{
    private String name;
    private int price;
    private int weight;

    public BaseProduct() {
        super();
        weight = 0;
    }

    public BaseProduct(String name, int price, int weight){
        this.name = name;
        this.price = price;
        this.weight = weight;
    }

    public BaseProduct(BaseProduct product) {
        this.name = product.getName();
        this.price = product.getPrice();
        this.weight = product.getWeight();
    }

    public void setName(String name) {this.name = name;}
    public void setPrice(int price) {this.price = price;}
    public void setWeight(int weight) {this.weight = weight;}

    public String getName() {return name;}
    public int getPrice() {return price;}
    public int getWeight() {return weight;}

    public int computePrice() {
        return getPrice();
    }

    public String toString() {
        return  "Prod. name: "+name+" Price: "+price + "Weight: " + weight;
    }
}
