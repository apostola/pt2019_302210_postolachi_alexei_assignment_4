package data;

public class MenuItem implements Product, java.io.Serializable  {
    private String name;
    private int price;

    public MenuItem() {
        name = "";
        price = 0;
    }

    public MenuItem(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {return name;}
    public int getPrice() {return price;}
    public void setName(String name) {this.name = name;}
    public void setPrice(int price) {this.price = price;}

    public int computePrice() { return price; }

    public String toString() { return "Name: " + name + " Price: " + price; }

    public boolean equals(MenuItem item) {
        if (name.equals(item.getName()) && price == item.computePrice()) {
            return true;
        }
        return false;
    }

}
