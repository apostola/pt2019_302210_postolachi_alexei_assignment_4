package data;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class Order {
    private int orderID;
    private Date date;
    private ArrayList<MenuItem> table;
    protected int price;

    public Order(int orderID) {
        this.orderID = orderID;
        this.date = new Date(System.currentTimeMillis());
        price = 0;
    }

    public void addItemToOrder(MenuItem item) {
        table.add(item);
        price += item.computePrice();
    }

    public void addItemsToOrder(ArrayList<MenuItem> orders) {
        table = orders;
        for (MenuItem order : orders) {
            price += order.computePrice();
        }
    }

    public void removeItem(MenuItem item) {
        int index = 0;
        for (MenuItem menuItem : table) {
            if (menuItem.equals(item)) {
                table.remove(index);
                price -= menuItem.computePrice();
                break;
            }
            index++;
        }
    }

    public ArrayList<MenuItem> getTable() { return table; }
    public int getOrderID() { return orderID; }
    public int getPrice() { return price; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return orderID == order.orderID &&
                date.equals(order.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, date);
    }
}
