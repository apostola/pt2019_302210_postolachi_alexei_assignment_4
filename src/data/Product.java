package data;

public interface Product {
    public int computePrice();
}
