package BLL;

import data.IRestaurantProcessing;
import data.MenuItem;
import data.Order;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class Restaurant extends Observable implements IRestaurantProcessing {
    private int orderID;
    private ArrayList<MenuItem> menu;
    private HashMap<Order, ArrayList<MenuItem>> orders;

    public Restaurant() {
        menu = new ArrayList<>();
        orders = new HashMap<>();
        orderID = 0;
        try {
            menu = loadMenuFromFile();
        } catch (Exception e) {
            System.out.println("Error on loading from file");
        }
    }

    public void saveMenuToFile() throws IOException {
        File file = new File("menu.sv");
        ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(file));
        stream.writeObject(menu);
    }

    public ArrayList<MenuItem> loadMenuFromFile() throws IOException, ClassNotFoundException {
        ObjectInputStream stream = new ObjectInputStream(new FileInputStream("menu.sv"));
        return (ArrayList<MenuItem>) stream.readObject();
    }

    @Override
    public void addItemToMenu(MenuItem item) {
        int size = menu.size();
        menu.add(item);
        assert size == menu.size() - 1 : "Error on insertion into menu!";
    }

    @Override
    public void deleteItemFromMenu(MenuItem item) {
        int size = menu.size();
        for (MenuItem itemFromMenu: menu) {
            if (itemFromMenu.equals(item)) {
                menu.remove(itemFromMenu);
                break;
            }
        }
        assert size == menu.size() + 1 : "Error while deleting object from menu!";
    }

    public void updateItem(String name, String newName, int price) {
        for (MenuItem item: menu) {
            if (item.getName().equals(name)) {
                item.setName(newName);
                item.setPrice(price);
            }
        }
    }

    public Order putOrder(ArrayList<MenuItem> items) {
        Order order = new Order(orderID);
        order.addItemsToOrder(items);
        orderID++;
        orders.put(order, order.getTable());
        setChanged();
        notifyObservers();

        return order;
    }

    public ArrayList<Order> getOrders() {
        ArrayList<Order> returnOrders = new ArrayList<>();
        for (Order o: orders.keySet()) {
            returnOrders.add(o);
        }

        return returnOrders;
    }

    public MenuItem getItem(String name) {
        for (MenuItem item: menu) {
            if (item.getName().equals(name))
                return item;
        }

        return null;
    }

    public ArrayList<MenuItem> getMenu() { return menu; }

    public void Receipt(Order order) throws IOException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String date = dateFormat.format(new Date());

        String path = order.getOrderID() + ".txt";
        FileOutputStream stream = new FileOutputStream(path);
        stream.write(("Date" + date).getBytes());
        stream.write("\n-----------------\n-----------------".getBytes());
        for (MenuItem item : order.getTable()) {
            stream.write((item.toString() + "\n").getBytes());
        }
        stream.write(("Total cost:" + order.getPrice()).getBytes());
    }
}
