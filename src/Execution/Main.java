package Execution;

import presentation.AdminUI;
import presentation.ChefUI;
import presentation.Controller;
import presentation.WaiterUI;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Controller controller;
        AdminUI adminUI = new AdminUI();
        WaiterUI waiterUI = new WaiterUI();

        controller = new Controller(adminUI, waiterUI);
    }
}
